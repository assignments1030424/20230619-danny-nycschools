package com.example.a20230618_danny_nycschools.network

import com.example.a20230618_danny_nycschools.data.SAT
import com.example.a20230618_danny_nycschools.data.Student
import retrofit2.http.GET

interface Api {

    @GET("resource/s3k6-pzi2.json?\$limit=10&\$offset=10")
    suspend fun getStudents(
    ): retrofit2.Response<ArrayList<Student>>

    @GET("resource/f9bf-2cp4.json")
    suspend fun getSatReults(
    ): retrofit2.Response<ArrayList<SAT>>

}