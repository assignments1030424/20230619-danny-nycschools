package com.example.a20230618_danny_nycschools.di

import android.content.Context
import com.example.a20230618_danny_nycschools.room.AppDatabase
import com.example.a20230618_danny_nycschools.room.StudentDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class DatabaseModule {

    @Singleton
    @Provides
    fun provideAppDatabase(@ApplicationContext context: Context): AppDatabase {
        return AppDatabase.getInstance(context)
    }

    @Provides
    fun provideUserDao(appDatabase: AppDatabase): StudentDao {
        return appDatabase.userDao()
    }

}