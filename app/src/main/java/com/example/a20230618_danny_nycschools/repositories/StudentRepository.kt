package com.example.a20230618_danny_nycschools.repositories

import com.example.a20230618_danny_nycschools.network.Api
import retrofit2.Retrofit
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class StudentRepository @Inject constructor(
    private val retrofit: Retrofit,
) {
    suspend fun getStudents(
    )  = retrofit.create(Api::class.java).getStudents()

    suspend fun getSat(
    )  = retrofit.create(Api::class.java).getSatReults()
}