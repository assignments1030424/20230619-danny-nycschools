package com.example.a20230618_danny_nycschools.utils

import android.app.Activity
import android.widget.Toast

fun Activity.showToast(message: String?, length: Int = Toast.LENGTH_LONG) {
    if (message != null)
        this.let { Toast.makeText(it, message, length).show() }
}