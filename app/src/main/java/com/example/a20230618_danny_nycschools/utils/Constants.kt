package  com.example.a20230618_danny_nycschools.utils

object Constants {

    const val BASE_URL =
        "https://data.cityofnewyork.us/"
    const val DATABASE_NAME = "school"
}