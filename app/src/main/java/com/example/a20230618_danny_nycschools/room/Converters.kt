package com.example.a20230618_danny_nycschools.room

import androidx.room.TypeConverter
import com.example.a20230618_danny_nycschools.data.Student
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken


class Converters {

    @TypeConverter
    fun dataToString(data: Student?): String? {
        return Gson().toJson(data)
    }

    @TypeConverter
    fun stringToData(string: String?): Student? {
        return Gson().fromJson(string, object : TypeToken<Student>() {}.type)
    }

}