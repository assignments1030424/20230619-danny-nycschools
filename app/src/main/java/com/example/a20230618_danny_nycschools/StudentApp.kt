package com.example.a20230618_danny_nycschools

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class StudentApp : Application(){
}