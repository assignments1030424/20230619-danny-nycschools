package com.example.a20230618_danny_nycschools.viewmodels

import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.a20230618_danny_nycschools.data.Student
import com.example.a20230618_danny_nycschools.repositories.StudentRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class StudentViewModel @Inject constructor(
    private val studentRepository: StudentRepository,
) : ViewModel() {

    private val _isLoading: MutableLiveData<Boolean> = MutableLiveData()
    val isLoading: LiveData<Boolean> = _isLoading

    private val _studentData: MutableLiveData<ArrayList<Student>> = MutableLiveData()
    val studentData: LiveData<ArrayList<Student>> = _studentData

    private val _errors: MutableLiveData<String> = MutableLiveData()
    val errors: LiveData<String> = _errors

    @SuppressLint("NullSafeMutableLiveData")
    fun getStudentData() {
        _isLoading.postValue(true)
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val response =
                    studentRepository.getStudents()
                if (response.isSuccessful) {
                    val studentData = response.body()
                    if (studentData != null) {
                        _studentData.postValue(studentData)
                    }
                } else {
                    _errors.postValue(response.errorBody()?.toString() ?: "")
                }
            } catch (e: Exception) {
                e.printStackTrace()
                _errors.postValue(e.localizedMessage)
            } finally {
                _isLoading.postValue(false)
            }
        }
    }

}

