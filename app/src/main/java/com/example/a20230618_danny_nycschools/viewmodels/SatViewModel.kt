package com.example.a20230618_danny_nycschools.viewmodels

import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.a20230618_danny_nycschools.data.SAT
import com.example.a20230618_danny_nycschools.repositories.StudentRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SatViewModel @Inject constructor(
    private val satRepository: StudentRepository,
) : ViewModel() {

    private val _isLoading: MutableLiveData<Boolean> = MutableLiveData()
    val isLoading: LiveData<Boolean> = _isLoading

    private val _satData: MutableLiveData<ArrayList<SAT>> = MutableLiveData()
    val satData: LiveData<ArrayList<SAT>> = _satData

    private val _errors: MutableLiveData<String> = MutableLiveData()
    val errors: LiveData<String> = _errors

    @SuppressLint("NullSafeMutableLiveData")
    fun getSatData() {
        _isLoading.postValue(true)
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val response =
                    satRepository.getSat()
                if (response.isSuccessful) {
                    val satData = response.body()
                    if (satData != null) {
                        _satData.postValue(satData)
                    }
                } else {
                    _errors.postValue(response.errorBody()?.toString() ?: "")
                }
            } catch (e: Exception) {
                e.printStackTrace()
                _errors.postValue(e.localizedMessage)
            } finally {
                _isLoading.postValue(false)
            }
        }
    }
}

