package com.example.a20230618_danny_nycschools.adapters


import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.a20230618_danny_nycschools.data.Student
import com.example.a20230618_danny_nycschools.databinding.ItemStudentBinding

class StudentAdapter(private var onItemClicked: ((student: Student) -> Unit)
) : ListAdapter<Student, StudentAdapter.ViewHolder>(
    StudentDiffUtils()
) {

    fun setOnItemClickListener(onItemClicked: ((Student) -> Unit)) {
        this.onItemClicked = onItemClicked
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemStudentBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class ViewHolder(private val mBinding: ItemStudentBinding) :
        RecyclerView.ViewHolder(mBinding.root) {

        fun bind(student: Student) {
            mBinding.student = student
            mBinding.rootLayout.setOnClickListener {
                onItemClicked.invoke(student)
            }
        }

    }
}

class StudentDiffUtils : DiffUtil.ItemCallback<Student>() {
    override fun areItemsTheSame(oldItem: Student, newItem: Student): Boolean {
        return oldItem.dbn == newItem.dbn
    }

    override fun areContentsTheSame(oldItem: Student, newItem: Student): Boolean {
        return oldItem == newItem
    }

}