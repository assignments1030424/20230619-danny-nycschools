package com.example.a20230618_danny_nycschools.adapters


import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.a20230618_danny_nycschools.data.SAT
import com.example.a20230618_danny_nycschools.databinding.ItemSatBinding

class SatAdapter(private var onItemClicked: ((SAT) -> Unit)) : ListAdapter<SAT, SatAdapter.ViewHolder>(
    SatDiffUtils()
) {

    fun setOnItemClickListener(onItemClicked: ((SAT) -> Unit)) {
        this.onItemClicked = onItemClicked
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemSatBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class ViewHolder(private val mBinding: ItemSatBinding) :
        RecyclerView.ViewHolder(mBinding.root) {

        fun bind(sat: SAT) {
            mBinding.sat = sat
            mBinding.rootLayout.setOnClickListener {
                onItemClicked.invoke(sat)
            }
        }

    }
}

class SatDiffUtils : DiffUtil.ItemCallback<SAT>() {
    override fun areItemsTheSame(oldItem: SAT, newItem: SAT): Boolean {
        return oldItem.dbn == newItem.dbn
    }

    override fun areContentsTheSame(oldItem: SAT, newItem: SAT): Boolean {
        return oldItem == newItem
    }

}