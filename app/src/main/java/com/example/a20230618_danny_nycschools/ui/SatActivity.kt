package com.example.a20230618_danny_nycschools.ui

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.a20230618_danny_nycschools.R
import com.example.a20230618_danny_nycschools.adapters.SatAdapter
import com.example.a20230618_danny_nycschools.data.SAT
import com.example.a20230618_danny_nycschools.databinding.ActivitySatBinding
import com.example.a20230618_danny_nycschools.utils.showToast
import com.example.a20230618_danny_nycschools.viewmodels.SatViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SatActivity : AppCompatActivity() {

    private lateinit var mBinding: ActivitySatBinding
    private val viewModel: SatViewModel by viewModels()
    private val satAdapter = SatAdapter(::onItemClick)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        //we are using data binding and view binding
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_sat)
        viewModel.getSatData()
        setUpObservers()
        satAdapter.setOnItemClickListener {
            //to handle click events in case we needed
        }
    }

    private fun setUpObservers() {
        viewModel.satData.observe(this) {
            val layoutManager = LinearLayoutManager(this)
            mBinding.satRv.layoutManager = layoutManager
            mBinding.satRv.adapter = satAdapter
            satAdapter.submitList(it)
        }
        viewModel.errors.observe(this) {
            // using extension function
            showToast(it)
        }
    }
    private fun onItemClick(sat: SAT) {
        // Handle the click event for the student item
    }
}