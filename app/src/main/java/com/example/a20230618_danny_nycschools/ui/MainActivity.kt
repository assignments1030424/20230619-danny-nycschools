package com.example.a20230618_danny_nycschools.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatDelegate
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.a20230618_danny_nycschools.R
import com.example.a20230618_danny_nycschools.adapters.StudentAdapter
import com.example.a20230618_danny_nycschools.data.Student
import com.example.a20230618_danny_nycschools.databinding.ActivityMainBinding
import com.example.a20230618_danny_nycschools.utils.showToast
import com.example.a20230618_danny_nycschools.viewmodels.StudentViewModel
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint // using hilt as Dependency Injection
class MainActivity : AppCompatActivity() {
    private  var mBinding: ActivityMainBinding? = null
    private val viewModel: StudentViewModel by viewModels()
    private val studentAdapter = StudentAdapter(::onItemClick)

    // We can also add bottom navigation and handle it with jetpack navigation if given enough time
    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        //we are using data binding and view binding
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        viewModel.getStudentData()
        setUpObservers()
        studentAdapter.setOnItemClickListener {
        }

        // if we had multiple clicks we can go with callbacks implementation to make code more clean
        mBinding?.goToSatBtn?.setOnClickListener {
            startActivity(Intent(this, SatActivity::class.java))
        }
    }

    private fun setUpObservers() {
        viewModel.studentData.observe(this) {
            val layoutManager = LinearLayoutManager(this)
            mBinding?.studentRv?.layoutManager = layoutManager
            mBinding?.studentRv?.adapter = studentAdapter
            studentAdapter.submitList(it)
        }
        viewModel.errors.observe(this) {
            // using extension function
            showToast(it)
        }
    }
    private fun onItemClick(student: Student) {
        // Handle the click event for the student item
    }
}